import React, { Component } from 'react';
import Inputlabel from './inputLabel';
import * as validateHelper from '../utils/validators';

class SignUpForm extends Component {
  constructor(props) {
    super(props);
    this.isError = false;
    this.state = {
      userName: '',
      email: '',
      birthDate: '',
      password: '',
      confirmPassword: '',
      errors: {
        userName: '',
        email: '',
        birthDate: '',
        password: '',
        confirmPassword: '',
      }
    }
  }

  checkErrors() {
    const { errors } = this.state;
    return !(errors.email || errors.password || errors.userName
      || errors.birthDate || errors.confirmPassword);
  }

  validateField = () => {
    let result = false;
    const errors = {
      userName: '',
      email: '',
      birthDate: '',
      password: '',
      confirmPassword: '',
    }
    const { userName, email, birthDate, password, confirmPassword } = this.state;
    if (validateHelper.validateEmail(email).isError) {
      errors.email = validateHelper.validateEmail(email).errorText;
      result = true;
    }

    if (validateHelper.validateName(userName)) {
      errors.userName = 'User name is required.';
      result = true;
    }
    if (validateHelper.validateDate(birthDate)) {
      errors.birthDate = 'Date is required.';
      result = true;
    }
    if (validateHelper.validatePass(password).isError) {
      errors.password = validateHelper.validatePass(password).errorText;
      result = true;
    }
    if (validateHelper.validateConfirmPass(confirmPassword, password)) {
      errors.confirmPassword = 'Password is not matched.';
      result = true;
    }
    this.setState({
      errors: errors
    });
    return result;
  }

  handleSubmit = (e) => {
    e.preventDefault();
    if (!this.validateField()) {
      // Send reques
      console.log('SUBMIT')
    }
  }

  onChangeHandler = e => {
    if (this.checkErrors()) {
      this.setState({
        [e.target.name]: e.target.value
      });
    } else {
      this.setState({
        [e.target.name]: e.target.value
      }, this.validateField);
    }
  }

  render() {
    return (
      <div className="container col-sm-8 col-md-6 col-xl-4 mt-5">
        <form onSubmit={this.handleSubmit} noValidate>
          <Inputlabel
            name='email'
            value={this.state.email}
            label='Email'
            type='text'
            placeholder='email'
            wrapperClass='mt-2'
            onChange={this.onChangeHandler}
            error={!!this.state.errors.email}
            errorText={this.state.errors.email}
          />
          <Inputlabel
            name='userName'
            value={this.state.userName}
            label='User name'
            type='text'
            placeholder='User name'
            wrapperClass='mt-2'
            onChange={this.onChangeHandler}
            error={!!this.state.errors.userName}
            errorText={this.state.errors.userName}
          />
          <Inputlabel
            name='birthDate'
            value={this.state.birthDate}
            label='Date of Birth'
            type='date'
            placeholder='email'
            wrapperClass='mt-2'
            onChange={this.onChangeHandler}
            error={!!this.state.errors.birthDate}
            errorText={this.state.errors.birthDate}
          />
          <Inputlabel
            value={this.state.password}
            name='password'
            type='password'
            label='Password'
            placeholder='Password'
            wrapperClass='mt-2'
            onChange={this.onChangeHandler}
            error={!!this.state.errors.password}
            errorText={this.state.errors.password}
          />
          <Inputlabel
            value={this.state.confirmPassword}
            name='confirmPassword'
            type='password'
            label='Confirm password'
            placeholder='Password'
            wrapperClass='mt-2'
            onChange={this.onChangeHandler}
            error={!!this.state.errors.confirmPassword}
            errorText={this.state.errors.confirmPassword}
          />
          <button className='btn btn-primary mt-5' variant="primary" type="submit">
            Submit
          </button>
        </form>
    </div>
    )
  }
}

export default SignUpForm;