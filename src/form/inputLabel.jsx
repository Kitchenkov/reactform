import React from 'react';
import PropTypes from 'prop-types'; 

function InputLabel(props) {
  const { value, name, onChange, labelClass, wrapperClass, label, type, placeholder, error, errorText } = props;
  return (
    <div className={wrapperClass + (!!error ? ' error' : '')}>
      {
        label &&
        <label className={labelClass}>{label}</label>
      }
      <div>
        <input
          name={name}
          className="form-control"
          value={value}
          type={type}
          onChange={onChange}
          placeholder={placeholder}
        />
        {error &&
          <div className="text-danger">{errorText}</div>
        }
      </div>
    </div>
  )
};

InputLabel.propTypes = {
  value: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  labelClass: PropTypes.string,
  wrapperClass: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  error: PropTypes.bool,
  errorText: PropTypes.string,
};

InputLabel.defaultProps = {
  value: '',
  label: '',
  type: 'text',
  placeholder: '',
  error: null,
  errorText: '',
  wrapperClass: '',
  labelClass: '',
  onChange: () => {
  }
};

export default InputLabel;