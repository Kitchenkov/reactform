import validator from 'email-validator';

export const validateEmail = (val) => {
  let errorText = '';
  let isError = false;
  if (val.length === 0) {
    errorText = 'Email is required.'
    isError = true;
  } else if (!validator.validate(val)) {
    errorText = 'Your email address is invalid.'
    isError = true;
  }
  return {
    isError: isError,
    errorText: errorText,
  }
}

export const validateName = (val) => {
  return !val.trim()
}

export const validateDate = (val) => {
  return !val.trim()
}

export const validatePass = (val) => {
  let errorText = '';
  let isError = false;
  if (val.length < 6) {
    errorText = 'Password can not be less 6 characters.'
    isError = true;
  }
  return {
      isError: isError,
      errorText: errorText,
  }
}

export const validateConfirmPass = (val, compareVal) => {
  return val !== compareVal;
}